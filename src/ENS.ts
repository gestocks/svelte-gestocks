// You can also use the generator at https://skeleton.dev/docs/generator to create these values for you
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';
export const ENS: CustomThemeConfig = {
	name: 'ENS',
	properties: {
		// =~= Theme Properties =~=
		'--theme-font-family-base': `system-ui`,
		'--theme-font-family-heading': `system-ui`,
		'--theme-font-color-base': '0 0 0',
		'--theme-font-color-dark': '255 255 255',
		'--theme-rounded-base': '4px',
		'--theme-rounded-container': '2px',
		'--theme-border-base': '0px',
		// =~= Theme On-X Colors =~=
		'--on-primary': '255 255 255',
		'--on-secondary': '0 0 0',
		'--on-tertiary': '0 0 0',
		'--on-success': '0 0 0',
		'--on-warning': '0 0 0',
		'--on-error': '0 0 0',
		'--on-surface': '0 0 0',
		// =~= Theme Colors  =~=
		// primary | #00778B
		'--color-primary-50': '217 235 238', // #d9ebee
		'--color-primary-100': '204 228 232', // #cce4e8
		'--color-primary-200': '191 221 226', // #bfdde2
		'--color-primary-300': '153 201 209', // #99c9d1
		'--color-primary-400': '77 160 174', // #4da0ae
		'--color-primary-500': '0 119 139', // #00778B
		'--color-primary-600': '0 107 125', // #006b7d
		'--color-primary-700': '0 89 104', // #005968
		'--color-primary-800': '0 71 83', // #004753
		'--color-primary-900': '0 58 68', // #003a44
		// secondary | #FFB432
		'--color-secondary-50': '255 244 224', // #fff4e0
		'--color-secondary-100': '255 240 214', // #fff0d6
		'--color-secondary-200': '255 236 204', // #ffeccc
		'--color-secondary-300': '255 225 173', // #ffe1ad
		'--color-secondary-400': '255 203 112', // #ffcb70
		'--color-secondary-500': '255 180 50', // #FFB432
		'--color-secondary-600': '230 162 45', // #e6a22d
		'--color-secondary-700': '191 135 38', // #bf8726
		'--color-secondary-800': '153 108 30', // #996c1e
		'--color-secondary-900': '125 88 25', // #7d5819
		// tertiary | #508CD2
		'--color-tertiary-50': '229 238 248', // #e5eef8
		'--color-tertiary-100': '220 232 246', // #dce8f6
		'--color-tertiary-200': '211 226 244', // #d3e2f4
		'--color-tertiary-300': '185 209 237', // #b9d1ed
		'--color-tertiary-400': '133 175 224', // #85afe0
		'--color-tertiary-500': '80 140 210', // #508CD2
		'--color-tertiary-600': '72 126 189', // #487ebd
		'--color-tertiary-700': '60 105 158', // #3c699e
		'--color-tertiary-800': '48 84 126', // #30547e
		'--color-tertiary-900': '39 69 103', // #274567
		// success | #82BE32
		'--color-success-50': '236 245 224', // #ecf5e0
		'--color-success-100': '230 242 214', // #e6f2d6
		'--color-success-200': '224 239 204', // #e0efcc
		'--color-success-300': '205 229 173', // #cde5ad
		'--color-success-400': '168 210 112', // #a8d270
		'--color-success-500': '130 190 50', // #82BE32
		'--color-success-600': '117 171 45', // #75ab2d
		'--color-success-700': '98 143 38', // #628f26
		'--color-success-800': '78 114 30', // #4e721e
		'--color-success-900': '64 93 25', // #405d19
		// warning | #F0825A
		'--color-warning-50': '253 236 230', // #fdece6
		'--color-warning-100': '252 230 222', // #fce6de
		'--color-warning-200': '251 224 214', // #fbe0d6
		'--color-warning-300': '249 205 189', // #f9cdbd
		'--color-warning-400': '245 168 140', // #f5a88c
		'--color-warning-500': '240 130 90', // #F0825A
		'--color-warning-600': '216 117 81', // #d87551
		'--color-warning-700': '180 98 68', // #b46244
		'--color-warning-800': '144 78 54', // #904e36
		'--color-warning-900': '118 64 44', // #76402c
		// error | #F01E28
		'--color-error-50': '253 221 223', // #fddddf
		'--color-error-100': '252 210 212', // #fcd2d4
		'--color-error-200': '251 199 201', // #fbc7c9
		'--color-error-300': '249 165 169', // #f9a5a9
		'--color-error-400': '245 98 105', // #f56269
		'--color-error-500': '240 30 40', // #F01E28
		'--color-error-600': '216 27 36', // #d81b24
		'--color-error-700': '180 23 30', // #b4171e
		'--color-error-800': '144 18 24', // #901218
		'--color-error-900': '118 15 20', // #760f14
		// surface | #BEBEBE
		'--color-surface-50': '245 245 245', // #f5f5f5
		'--color-surface-100': '242 242 242', // #f2f2f2
		'--color-surface-200': '239 239 239', // #efefef
		'--color-surface-300': '229 229 229', // #e5e5e5
		'--color-surface-400': '210 210 210', // #d2d2d2
		'--color-surface-500': '190 190 190', // #BEBEBE
		'--color-surface-600': '171 171 171', // #ababab
		'--color-surface-700': '143 143 143', // #8f8f8f
		'--color-surface-800': '114 114 114', // #727272
		'--color-surface-900': '93 93 93' // #5d5d5d
	}
};
